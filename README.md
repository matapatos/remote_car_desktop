# Remote car desktop (brain) #

This project contains the source code for controlling an RC car using vision.

## Frameworks used ##

1. [Pytorch](https://pytorch.org/) - Deep learning framework used for controlling the RC car. It's used Convolutional Neural Netwoks.
2. [Robot Operating System (ROS)](https://www.ros.org/) - Framework used for communicating with Android device. To send the commands as well as to receive the stream from the Smartphone camera.

## Creating training/testing dataset architecture ##

For creating the *training* and *testing* datasets the user needs to drive the car via (with up/down/left/right arrow keys), which will record the streamed images and associate them with the keys pressed (see GIF example bellow).
In the end, this will generate a video file (set of images) with a corresponding labels file that contains the corresponding keys pressed for each image.

![Training sample](imgs/training_sample.gif)

![Workflow for creating a training/testing dataset](imgs/training.png)


## Autonomous driving architecture ##

The current implementation controls the RC car using the vision system. It receives the streamed images from the Smartphone Camera, and according to that, it sends back the predicted commands.

![Workflow autonomous driving](imgs/driving.png)