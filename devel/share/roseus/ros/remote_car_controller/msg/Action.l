;; Auto-generated. Do not edit!


(when (boundp 'remote_car_controller::Action)
  (if (not (find-package "REMOTE_CAR_CONTROLLER"))
    (make-package "REMOTE_CAR_CONTROLLER"))
  (shadow 'Action (find-package "REMOTE_CAR_CONTROLLER")))
(unless (find-package "REMOTE_CAR_CONTROLLER::ACTION")
  (make-package "REMOTE_CAR_CONTROLLER::ACTION"))

(in-package "ROS")
;;//! \htmlinclude Action.msg.html


(defclass remote_car_controller::Action
  :super ros::object
  :slots (_action _value ))

(defmethod remote_car_controller::Action
  (:init
   (&key
    ((:action __action) 0)
    ((:value __value) 0.0)
    )
   (send-super :init)
   (setq _action (round __action))
   (setq _value (float __value))
   self)
  (:action
   (&optional __action)
   (if __action (setq _action __action)) _action)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; int8 _action
    1
    ;; float32 _value
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int8 _action
       (write-byte _action s)
     ;; float32 _value
       (sys::poke _value (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int8 _action
     (setq _action (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _action 127) (setq _action (- _action 256)))
   ;; float32 _value
     (setq _value (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get remote_car_controller::Action :md5sum-) "cb494c3172065ab8923eedda9e145278")
(setf (get remote_car_controller::Action :datatype-) "remote_car_controller/Action")
(setf (get remote_car_controller::Action :definition-)
      "int8 action
float32 value

")



(provide :remote_car_controller/Action "cb494c3172065ab8923eedda9e145278")


