

class ReleaseEventNotFoundError(Exception):
    '''Exception when a release event is not found'''
    pass


class NotADirectoryError(Exception):
    pass