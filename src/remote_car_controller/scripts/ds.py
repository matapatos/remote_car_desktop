import os
import glob
from torch.utils.data import Dataset
from pathlib import Path
import cv2
import numpy as np
import keyboard


from configs import Commands


class SelfDrivingRCCarDataset(Dataset):
    def __init__(self, dataset_folder):
        self.dataset_folder = dataset_folder
        self.data = self._get_all_data()
        self._num_frames_per_video, self._total_num_of_frames = self._get_num_frames()

    def _get_all_data(self):
        '''
        Returns {}

        Where as keys() has the filepaths of the videos and in the values() has the corresponding label filepath
        '''
        if not self.dataset_folder or not os.path.isdir(self.dataset_folder):
            raise NotADirectoryError('%s' % (self.dataset_folder))

        # Get all videos from the folder
        input_filespath = glob.glob(os.path.join(self.dataset_folder, '*.avi'))
        input_filespath.sort()
        # Get all the labels from the folder
        labels_filepath = glob.glob(os.path.join(self.dataset_folder, '*.npy'))
        labels_filepath.sort()
        assert(len(input_filespath) == len(labels_filepath))

        # Join both
        data = dict(zip(input_filespath, labels_filepath))
        for input_, label in data.iteritems():
            assert(Path(input_).stem == Path(label).stem)  # Make sure that input and label file has the same name
        return data

    def _get_num_frames(self):
        '''
        Returns {}, int

        {}  -> contains as keys() the filepath of the videos and as values() the number of frames contained on that video
        int -> corresponds to the total of frames in the whole videos
        '''
        if not self.data:
            return {}, 0

        num_frames_per_video = []
        total_num_of_frames = 0
        for video, labels in self.data.iteritems():
            cap = cv2.VideoCapture(video)
            num_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)  # Get number of frames contained in video
            cap.release()

            num_frames_per_video.append(num_frames) 
            total_num_of_frames += num_frames

        return dict(zip(self.data.keys(), num_frames_per_video)), total_num_of_frames

    def _get_video_labels_and_frame_index_from_index(self, idx):
        if idx >= self._total_num_of_frames or idx < 0:
            raise IndexError('%s' % (idx))

        total = 0
        for video, num_frames in self._num_frames_per_video.iteritems():
            total += num_frames
            if idx < total:
                frame_idx = int(idx - (total - num_frames))
                return video, self.data[video], frame_idx
        else:
            raise IndexError('Index not found %s' % (idx))

    def _convert_commands_to_label(self, cmds):
        final_label = np.zeros(shape=(4,))
        if Commands.ACCELERATE.value in cmds:
            final_label[0] = 1
        if Commands.DECELERATE.value in cmds:
            final_label[1] = 1
        if Commands.TURN_RIGHT.value in cmds:
            final_label[2] = 1
        if Commands.TURN_LEFT.value in cmds:
            final_label[3] = 1
        return final_label

    def __len__(self):
        return self._total_num_of_frames

    def __getitem__(self, idx):
        video_path, labels_path, frame_idx = self._get_video_labels_and_frame_index_from_index(idx)

        # Get frame from video
        cap = cv2.VideoCapture(video_path)
        cap.set(cv2.CAP_PROP_POS_FRAMES, frame_idx)  # Set frame index
        _, frame = cap.read()
        cap.release()
        frame = np.reshape(frame, newshape=(3, 176, 144))
        # Load labels
        all_labels = np.load(labels_path)
        label = self._convert_commands_to_label(all_labels[frame_idx])

        return np.asarray(frame, dtype=np.float32), np.asarray(label, dtype=np.float32)


class CreateDSFunctionality():
    '''
    Class containing all the required auxiliary methods for creating a dataset
    '''
    def _handle_key_presses(self):
        # Key presses
        keyboard.on_press_key('up',
                              self._on_key_press,
                              suppress=False)
        keyboard.on_press_key('down',
                              self._on_key_press,
                              suppress=False)
        keyboard.on_press_key('right',
                              self._on_key_press,
                              suppress=False)
        keyboard.on_press_key('left',
                              self._on_key_press,
                              suppress=False)
        # Key releases
        keyboard.on_release_key('up',
                                self._on_key_release,
                                suppress=False)
        keyboard.on_release_key('down',
                                self._on_key_release,
                                suppress=False)
        keyboard.on_release_key('right',
                                self._on_key_release,
                                suppress=False)
        keyboard.on_release_key('left',
                                self._on_key_release,
                                suppress=False)

    def _on_key_press(self, key):
        command = self._convert_key_to_command(key)
        if command > 0:                
            self.publisher_driver.publish(command)

    def _on_key_release(self, key):
        if hasattr(key, 'name'):
            key = key.name

        if key == 'up':
            command = Commands.STOP_ACCELERATE.value
        elif key == 'down':
            command = Commands.STOP_DECELERATE.value
        elif key == 'right':
            command = Commands.STOP_RIGHT.value
        elif key == 'left':
            command = Commands.STOP_LEFT.value
        else:
            raise ValueError("Invalid key given %s" % (key))

        self.publisher_driver.publish(command)

    def _transform_keyboard_events_into_labels(self, screen, keyboard_events):
        keys_played = []
        for i, event in enumerate(keyboard_events):
            if event.name in ['up', 'down', 'right', 'left']:
                if event.event_type == 'down':  # Pressed
                    release_event = self._get_release_event(start_event=event,
                                                            start_searching_index=i + 1,
                                                            events=keyboard_events)
                    keys_played.append({'name': event.name,
                                        'start_time': event.time,
                                        'release_time': release_event.time})

        # Convert to binary array
        labels = [[] for _ in range(len(screen.time_frames))]
        for key in keys_played:
            start_index, end_index = self._get_frame_indexes(screen=screen,
                                                             start_time=key['start_time'],
                                                             release_time=key['release_time'])
            # Update labels
            for i in range(start_index, end_index):
                cmd = self._convert_key_to_command(key['name'])
                if cmd not in labels[i]:
                    labels[i].append(cmd)

        # Make sure that array has 4 columns per each row
        for i, l in enumerate(labels):
            size = len(l)
            if size < 4:
                l.extend([0 for _ in range(4 - size)])
                labels[i] = l

        return np.asarray(labels)

    def _get_frame_indexes(self, screen, start_time, release_time):
        '''
        First index refers to the start_time and is inclusive and the other one not.
        '''
        for i, t in enumerate(screen.time_frames):
            if start_time <= t:
                for j in range(i + 1, len(screen.time_frames)):
                    if screen.time_frames[j] > release_time:
                        return i, j

        assert(i < len(screen.time_frames))
        return i, j + 1  # Return j, as the last frame


    def _get_release_event(self, start_event, start_searching_index, events):
        for i in range(start_searching_index, len(events)):    
            e = events[i]
            if e.name == start_event.name and e.event_type == 'up':
                return e

        raise ReleaseEventNotFoundError('%s' % (start_event))

    def _convert_key_to_command(self, key):
        if hasattr(key, 'name'):
            key = key.name

        if key == 'up':
            command = Commands.ACCELERATE.value
        elif key == 'down':
            command = Commands.DECELERATE.value
        elif key == 'right':
            command = Commands.TURN_RIGHT.value
        elif key == 'left':
            command = Commands.TURN_LEFT.value
        else:
            raise ValueError("Invalid key given %s" % (key))

        return command

