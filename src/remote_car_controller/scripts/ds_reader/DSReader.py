
import os


import Globals as rc_globals
from ds_reader.generators.SequencialGenerator import SequencialGenerator


class DSReader():

    def __init__(self, ds_original_folder, ds_expected_folder):
        self.ds_original_folder = ds_original_folder
        self.ds_expected_folder = ds_expected_folder
        self.original_files = None
        self.expected_files = None
        self.generator = None

    def __prepare_DS(self):
        if not os.path.isdir(self.ds_original_folder):
            rc_globals.ERROR('\'' + str(self.ds_original_folder) + '\' doesn\'t exists.')

        if not os.path.isdir(self.ds_expected_folder):
            rc_globals.ERROR('\'' + str(self.ds_expected_folder) + '\' doesn\'t exists.')
        
        self.original_files = os.listdir(self.ds_original_folder)
        if len(self.original_files) == 0:
            rc_globals.ERROR('\'' + str(self.ds_original_folder) + '\' it\'s empty.')

        self.expected_files = os.listdir(self.ds_expected_folder)
        if len(self.expected_files) == 0:
            rc_globals.ERROR('\'' + str(self.ds_expected_folder) + '\' it\'s empty.')

        if len(self.original_files) != len(self.expected_files):
            rc_globals.WARN('The original DS has a different number of images than the expected DS. Original ds has ' + str(len(self.original_files)) + ' and expected ds has ' + str(len(self.expected_files)))

        self.original_files = self.__append_fullpath(folder_path=rc_globals.ORIGINAL_FOLDER_DS,
                                                    filenames=self.original_files)
        self.expected_files = self.__append_fullpath(folder_path=rc_globals.EXPECTED_FOLDER_DS,
                                                    filenames=self.expected_files)

    
    def __unprepare_DS(self):
        self.original_files = None
        self.expected_files = None
        self.generator = None

    def __append_fullpath(self, folder_path, filenames):
        fullpath_files = []
        for f in filenames:
            fullpath_files.append(os.path.join(folder_path, f))
        return fullpath_files

    def __iter__(self):
        self.generator = SequencialGenerator(original_ds_files=self.original_files,
                                             expected_ds_files=self.expected_files)
        return self.generator

    def __enter__(self):
        self.__prepare_DS()
        return self

    def __exit__(self, exc_t, exc_v, trace):
        self.__unprepare_DS()
