from PIL import Image


class DSGenerator():

    def __init__(self, original_ds_files, expected_ds_files):
        self.original_ds_files = original_ds_files
        self.expected_ds_files = expected_ds_files

        self.reset()

    def reset(self):
        self.index = 0

    def _get_original_image_by_index(self, index):
        return self.__get_image_from_files_list_by_index(files_list=self.original_ds_files,
                                                         index=self.index)

    def _get_expected_image_by_index(self, index):
        return self.__get_image_from_files_list_by_index(files_list=self.expected_ds_files,
                                                         index=self.index)

    def __get_image_from_files_list_by_index(self, files_list, index):
        image_filepath = files_list[index]
        return Image.open(image_filepath, mode='r')

    def __iter__(self):
        return self

    def next(self):
        raise NotImplementedError