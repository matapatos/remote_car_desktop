
from ds_reader.generators.DSGenerator import DSGenerator


class SequencialGenerator(DSGenerator):

    def __init__(self, original_ds_files, expected_ds_files):
        DSGenerator.__init__(self,
                             original_ds_files=original_ds_files,
                             expected_ds_files=expected_ds_files)

    def next(self):
        if len(self.original_ds_files) <= self.index or len(self.expected_ds_files) <= self.index:
            raise StopIteration
        
        orig = self._get_original_image_by_index(index=self.index)
        expected = self._get_expected_image_by_index(index=self.index)
        self.index += 1

        return orig, expected