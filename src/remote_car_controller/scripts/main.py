#!/usr/bin/env python
import remote_car as rc
import argparse
import os
import sys
import time


class Main():

    def __init__(self):
        self.car = rc.Car()
        self.start_time = time.time()

    def start(self):
        args = self.__parse_arguments()

        if args.drive:
            self.car.drive(args.model)

        elif args.create_ds:
            self.car.create_ds(args.output_folder)

        elif args.training:
            self.car.learn(args.dataset_folder)

    def __parse_arguments(self):
        parser = argparse.ArgumentParser(description='Available arguments:')

        algo_group = parser.add_mutually_exclusive_group(required=True)
        # Main options
        algo_group.add_argument('-d', '-D', '--drive', 
                                help='Autonomous driving mode (its required a model for this option)', 
                                action='store_const', 
                                const=True)
        algo_group.add_argument('-c', '-C', '--create_ds', 
                                help='Create dataset mode', 
                                action='store_const', 
                                const=True)
        algo_group.add_argument('-t', '-T', '--training', 
                                help='Training mode', 
                                action='store_const', 
                                const=True)

        # Sub-options
        parser.add_argument('-m', '-M', '--model',
                            help='Model filepath to use for autonomous driving')
        parser.add_argument('-o', '-O', '--output_folder',
                            help='Output folder to save the Dataset')
        parser.add_argument('-ds', '-DS', '--dataset_folder',
                            help='Dataset folder containing /train, /test and /eval [Optional] sub-folders')

        args = parser.parse_args()
        # Additional checks
        if args.drive:
            usage = 'usage: ' + __file__ + ' --drive -m [MODELPATH]\n\n'
            if args.model is None:
                print(usage + '-m/-M/--model is required for autonomous driving mode')
                sys.exit(-1)

            elif not os.path.isfile(args.model):
                print(usage + '-m/-M/--model must be an existent file')
        elif args.create_ds:
            usage = 'usage: ' + __file__ + ' --create_ds -o [OUTPUT_FOLDERPATH]\n\n'
            if args.output_folder is None:
                print(usage + '-o/-O/--output_folder is required')
                sys.exit(-1)
            elif not os.path.isdir(args.output_folder):
                print(usage + '-o/-O/--output_folder must be an existent folder')
                sys.exit(-1)
        elif args.training:
            usage = 'usage: ' + __file__ + ' --training -t -ds [DATASET_FOLDER]\n\n'
            if not args.dataset_folder or not os.path.isdir(args.dataset_folder):
                print(usage + '-ds/-DS/--dataset_folder must be an existent folder')
                sys.exit(-1)

        return args


if __name__ == "__main__":
    a = Main()
    a.start()