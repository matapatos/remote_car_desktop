
import os
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.nn.functional as F
from ignite.engine import Events,\
                          create_supervised_trainer,\
                          create_supervised_evaluator
from ignite.metrics import Accuracy,\
                           Loss,\
                           MetricsLambda,\
                           Precision,\
                           Recall
from ignite.handlers.early_stopping import EarlyStopping
from ignite.handlers.checkpoint import ModelCheckpoint


from ds import SelfDrivingRCCarDataset
from auxiliary.adamwr.adamw import AdamW
from auxiliary.adamwr.cyclic_scheduler import CyclicLRWithRestarts
from utils import get_device
from custom_exceptions import NotADirectoryError


class PytorchIgnite():
    def _get_supervised_trainer(self, model, dataloader_len, batch_size=8, lr=1e-3, weight_decay=1e-5, checkpoint_filepath='./models'):
        optimizer = AdamW(model.parameters(), lr=lr, weight_decay=weight_decay)
        scheduler = CyclicLRWithRestarts(optimizer,
                                         batch_size,
                                         dataloader_len * batch_size,
                                         restart_period=5,
                                         t_mult=1.2,
                                         policy="cosine") 
        trainer = create_supervised_trainer(model, optimizer, F.binary_cross_entropy, device=get_device())
        trainer.add_event_handler(Events.ITERATION_COMPLETED, lambda engine: scheduler.batch_step())
        trainer.add_event_handler(Events.EPOCH_STARTED, lambda engine: scheduler.step())
        checkpoint = ModelCheckpoint(dirname=checkpoint_filepath,
                                     filename_prefix='checkpoint',
                                     n_saved=1,
                                     save_interval=1,
                                     create_dir=True,
                                     require_empty=False)
        trainer.add_event_handler(Events.EPOCH_COMPLETED, checkpoint, {'model': model,
                                                                       'optimizer': optimizer,
                                                                       'lr_scheduler': scheduler})
        return trainer

    def _get_supervised_evaluator(self, model, trainer, use_early_stopping=True, output_filepath='./models'):
        # Define metrics or auxiliary functions for metrics
        def convert_to_binary_preds(data):
            y_pred, y = data
            y_pred = torch.round(y_pred)
            return y_pred, y

        precision = Precision(output_transform=convert_to_binary_preds,
                              average=False,
                              is_multilabel=True)
        recall = Recall(output_transform=convert_to_binary_preds,
                        average=False,
                        is_multilabel=True)
        F1 = (precision * recall * 2 / (precision + recall + 1e-20)).mean()
        # Create supervised evaluator
        evaluator = create_supervised_evaluator(model,
                                                metrics={'fmeasure' : F1,
                                                         'accuracy': Accuracy(output_transform=convert_to_binary_preds, is_multilabel=True),
                                                         'binary_cross_entropy': Loss(F.binary_cross_entropy)},
                                                device=get_device())
        # Save best epoch model
        def score_function(engine):
            return engine.state.metrics['fmeasure']
        checkpoint = ModelCheckpoint(dirname=output_filepath,
                                     filename_prefix='best',
                                     score_function=score_function,
                                     n_saved=1,
                                     create_dir=True,
                                     require_empty=False)
        evaluator.add_event_handler(Events.COMPLETED, checkpoint, {'model' : model})
        # Attach Early Stopping
        if use_early_stopping:
            def early_stop_fmeasure(engine):
                return -engine.state.metrics['fmeasure']

            evaluator.add_event_handler(Events.COMPLETED, handler=EarlyStopping(patience=10,
                                                                                score_function=early_stop_fmeasure,
                                                                                trainer=trainer))
        return evaluator

    def _get_train_and_eval_dataloaders(self, dataset_folder):
        train_ds_path, eval_ds_path, _ = self._get_train_eval_test_datasets_folder(dataset_folder)
        # Get Train DataLoader
        train_ds = SelfDrivingRCCarDataset(dataset_folder=train_ds_path)
        train_loader = DataLoader(train_ds, batch_size=8, num_workers=4)
        # Get Eval DataLoader
        eval_ds = SelfDrivingRCCarDataset(dataset_folder=eval_ds_path)
        eval_loader = DataLoader(eval_ds, batch_size=8, num_workers=4)
        return train_loader, eval_loader

    def _get_train_eval_test_datasets_folder(self, dataset_folder):
        if not os.path.isdir(dataset_folder):
            raise NotADirectoryError("%s" % (dataset_folder))

        training_ds = os.path.join(dataset_folder, 'train')
        if not os.path.isdir(training_ds):
            raise NotADirectoryError("%s" % (training_ds))

        testing_ds = os.path.join(dataset_folder, 'test')
        if not os.path.isdir(testing_ds):
            raise NotADirectoryError("%s" % (testing_ds))

        evaluation_ds = os.path.join(dataset_folder, 'eval')
        if not os.path.isdir(evaluation_ds):
            evaluation_ds = testing_ds

        return training_ds, evaluation_ds, testing_ds
