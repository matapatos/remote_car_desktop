import torch
from torch import nn
import torch.nn.functional as F


class CNN(nn.Module):
    def __init__(self, num_classes):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(3, 1, 3)
        self.pool1 = nn.MaxPool2d(8, 4)
        self.fc1 = nn.Linear(1*42*34, 64)
        self.fc2 = nn.Linear(64, num_classes)

    def forward(self, x):
        x = self.pool1(F.leaky_relu(self.conv1(x)))
        x = x.view(-1, 1*42*34)
        x = F.leaky_relu(self.fc1(x))
        x = torch.sigmoid(self.fc2(x))
        return x