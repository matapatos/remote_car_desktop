import rospy
import os
import cv2
import cv_bridge as cv_b
import keyboard
import time
import imutils
import numpy as np
import collections
import socketio
from ignite.contrib.handlers import ProgressBar
from ignite.engine import Events
import torch


from topics import StreamingCameraTopic,\
                   DriverTopic
from auxiliary.rwlock import RWLock
from ds import CreateDSFunctionality
from ml import PytorchIgnite
from custom_exceptions import ReleaseEventNotFoundError
from utils import Commands,\
                  exec_and_stop_commands
from net import CNN

np.random.seed(158)
torch.random.manual_seed(158)


class Car(CreateDSFunctionality, PytorchIgnite):

    def __init__(self):
        self.node_name = "remote_car_controller"
        self.publisher_driver = None
        self.previous_pred = np.zeros(shape=(4,))
        self.rwlock = RWLock()

    def create_ds(self, output_folder):
        self.publisher_driver = self._get_publisher_driver(avoid_repeated_requests=True)
        # Prepare node
        self._init_node()
        # Prepare screen streaming
        screen = Screen(video_output_path=os.path.join(output_folder, 'video.avi'))
        screen.subscribe()
        # Record keyboard
        self._handle_key_presses()        
        recorded_keyboard_events = keyboard.record(until='esc')
        # Terminate pending processes
        screen.stop_subscription()
        rospy.signal_shutdown('Create_DS mode terminated')
        self.publisher_driver.disconnect()
        # Create labels dataset
        labels = self._transform_keyboard_events_into_labels(screen=screen,
                                                             keyboard_events=recorded_keyboard_events)
        assert(len(labels) == len(screen.time_frames))
        np.save(file=os.path.join(output_folder, 'labels.npy'),
                arr=labels)

    def learn(self, dataset_folder, max_epochs=900):
        # Get DataLoaders
        train_loader, eval_loader = self._get_train_and_eval_dataloaders(dataset_folder=dataset_folder)
        # Create supervised trainer and evaluator
        model = CNN(num_classes=4)
        trainer = self._get_supervised_trainer(model=model,
                                               dataloader_len=len(train_loader),
                                               batch_size=train_loader.batch_size,
                                               lr=1e-3,
                                               weight_decay=1e-5)
        evaluator = self._get_supervised_evaluator(model=model,
                                                   trainer=trainer,
                                                   use_early_stopping=True)
        # Attach prograss bar to trainer
        pbar = ProgressBar(persist=True, bar_format="")
        pbar.attach(trainer)

        @trainer.on(Events.EPOCH_COMPLETED)
        def log_validation_results(engine):
            evaluator.run(eval_loader)
            metrics = evaluator.state.metrics
            avg_fmeasure = metrics['fmeasure']
            avg_accuracy = metrics['accuracy']
            avg_loss = metrics['binary_cross_entropy']
            pbar.log_message(
                "Validation Results - Epoch: {} Avg fmeasure: {:.6f} Avg accuracy: {:.6f} Avg loss: {:.6f}"
                .format(engine.state.epoch, avg_fmeasure, avg_accuracy, avg_loss))

            pbar.n = pbar.last_print_n = 0

        trainer.run(train_loader, max_epochs=max_epochs)
        pbar.close()

    def drive(self, model_filepath):
        self.publisher_driver = self._get_publisher_driver(avoid_repeated_requests=True)
        # Prepare node
        self._init_node()
        # Prepare model
        model = CNN(num_classes=4)
        model.load_state_dict(torch.load(model_filepath))
        def on_image_callback(frame):
            frame = np.reshape(frame, newshape=(3, 176, 144))
            y = model(torch.Tensor([frame]))
            cmds = self._convert_labels_to_commands(pred=y)
            for c in cmds:
                self.publisher_driver.publish(c)

        # Prepare screen streaming
        screen = Screen(on_image_callback=on_image_callback)
        screen.subscribe()
        # Record keyboard
        recorded_keyboard_events = keyboard.record(until='esc')
        # Terminate pending processes
        screen.stop_subscription()
        rospy.signal_shutdown('Create_DS mode terminated')
        self.publisher_driver.disconnect()

    def _init_node(self):
        rospy.init_node(self.node_name, anonymous=True)

    def _get_publisher_driver(self, avoid_repeated_requests):
        # topic = DriverTopic()
        # print('Publishing to %s' % (topic.name))
        # return rospy.Publisher(name=topic.name,
        #                        data_class=topic.msg_type,
        #                        queue_size=topic.queue_size)
        return SocketPublisher(avoid_repeated_requests=avoid_repeated_requests)

    def _convert_labels_to_commands(self, pred):
        pred = pred.data.cpu().numpy()
        pred = pred.reshape((4,))
        pred = np.round(pred)
        cmds = []
        with self.rwlock.r_locked():
            for p, previous, (cmd, stop_cmd) in zip(pred, self.previous_pred, exec_and_stop_commands.iteritems()):
                if p == 1:
                    cmds.append(cmd.value)
                elif previous == 1:
                    cmds.append(stop_cmd.value)

        with self.rwlock.w_locked():
            self.previous_pred = pred

        return cmds


class Screen():
    def __init__(self, video_output_path=None, video_format='XVID', fps=10, screen_dimensions=(144, 176), on_image_callback=None):
        self.topic = StreamingCameraTopic(callback=self.__on_new_image_callback)
        self.subscription = None
        self.bridge = cv_b.CvBridge()
        self.time_frames = None
        self.video_writer = None
        self.on_image_callback = on_image_callback
        if video_output_path:
            fourcc = cv2.VideoWriter_fourcc(*video_format)
            self.video_writer = cv2.VideoWriter(video_output_path,
                                                fourcc,
                                                fps,
                                                screen_dimensions)
        

    def subscribe(self):
        print('Subscribing to %s...' % (self.topic.name))
        self.time_frames = []
        self.subscription = rospy.Subscriber(name=self.topic.name,
                                             data_class=self.topic.msg_type,
                                             callback=self.topic.callback,
                                             queue_size=1)

    def stop_subscription(self):
        if self.subscription:
            self.subscription.unregister()

        if self.video_writer and self.video_writer.isOpened():
            self.video_writer.release()

    def __on_new_image_callback(self, data):
        self.time_frames.append(time.time())
        try:
            frame = self.bridge.compressed_imgmsg_to_cv2(data, "bgr8")
            frame = imutils.rotate_bound(frame, 90)
            cv2.imshow('Frame',frame)
            if cv2.waitKey(1) == 27:
                self.stop_subscription()

            if self.video_writer:
                self.video_writer.write(frame)
            
            if self.on_image_callback:
                self.on_image_callback(frame)

        except cv_b.CvBridgeError as e:
            rospy.logerr('[RC Screen] Converting Image Error. ' + str(e))


class SocketPublisher():
    def __init__(self, avoid_repeated_requests):
        self.sio = socketio.Client()
        self.sio.connect('http://localhost:5000', namespaces=['/driver'])
        self.avoid_repeated_requests = avoid_repeated_requests
        # Avoid repeated requests
        self.__forward_backward_cmd = None
        self.__steer_cmd = None
        self.__forward_backward_lock = RWLock()
        self.__steer_lock = RWLock()

    def publish(self, cmd):
        if self.avoid_repeated_requests and self.__is_repeated_request(cmd=cmd):
            return

        self.sio.emit('cmd', int(cmd), namespace='/driver')

    def disconnect(self):
        self.sio.disconnect()

    def __is_repeated_request(self, cmd):
        if self.__is_steer_cmd(cmd):
            with self.__steer_lock.r_locked():
                is_same_value = True if cmd == self.__steer_cmd else False
            
            if not is_same_value:
                with self.__steer_lock.w_locked():
                    self.__steer_cmd = cmd
        else:
            with self.__forward_backward_lock.r_locked():
                is_same_value = True if cmd == self.__forward_backward_cmd else False
            
            if not is_same_value:
                with self.__forward_backward_lock.w_locked():
                    self.__forward_backward_cmd = cmd

        return is_same_value
    
    def __is_steer_cmd(self, cmd):
        if cmd == Commands.ACCELERATE.value or cmd == Commands.DECELERATE.value or \
           cmd == Commands.STOP_ACCELERATE.value or cmd == Commands.STOP_DECELERATE.value:
            return False
        return True

