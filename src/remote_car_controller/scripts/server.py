import socketio
from flask import Flask


sio = socketio.Server(async_mode='threading')
app = Flask(__name__)
app.wsgi_app = socketio.Middleware(sio, app.wsgi_app)

# Driver handlers

@sio.on('connect', namespace='/driver')
def on_connect(sid, environ):
    print('[DRIVER] Connected %s...' % (sid))

@sio.on('cmd', namespace='/driver')
def ondriver(sid, data):
    sio.emit('drivercommands', data, namespace='/car')

@sio.on('disconnect', namespace='/driver')
def disconnect(sid):
    print('[DRIVER] Disconnected...')
    sio.emit('driverdisconnected', 0, namespace='/car')

# Car handlers
@sio.on('connect', namespace='/car')
def on_connect(sid, environ):
    print('[CAR] Connected %s...' % (sid))

@sio.on('disconnect', namespace='/car')
def disconnect(sid):
    print('[CAR] Disconnected...')


if __name__ == '__main__':
    app.run(threaded=True, host='0.0.0.0')
