import rospy
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import Int8

class Topic():

    def __init__(self, name, msg_type):
        self.name = name
        self.msg_type = msg_type

class SubscriberTopic(Topic):

    def __init__(self, name, msg_type, callback):
        Topic.__init__(self, name=name, msg_type=msg_type)
        
        self.callback = callback

    def callback(self, data):
        self.callback(data)

''' To visualize data:
   -> Run the following command: rosrun web_video_server web_video_server
       -> Open browser in http://localhost:8080/stream_viewer?topic=/camera/image&type=ros_compressed
'''
class StreamingCameraTopic(SubscriberTopic):

    def __init__(self, callback):
        SubscriberTopic.__init__(self, name="/camera/image/compressed", msg_type=CompressedImage, callback=callback)

class PublisherTopic(Topic):

    def __init__(self, name, msg_type, queue_size):
        Topic.__init__(self, name=name, msg_type=msg_type)
        self.queue_size = queue_size

class DriverTopic(PublisherTopic):

    def __init__(self):
        PublisherTopic.__init__(self, name='/remote/car/driver', msg_type=Int8, queue_size=1)
