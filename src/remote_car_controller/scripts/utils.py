import torch
from enum import Enum
import collections


class Commands(Enum):
    ACCELERATE = 1
    DECELERATE = 2
    STOP_ACCELERATE = 3
    STOP_DECELERATE = 4
    TURN_RIGHT = 5
    TURN_LEFT = 6
    STOP_RIGHT = 7
    STOP_LEFT = 8
    STOP_CAR = 9


exec_and_stop_commands = collections.OrderedDict()
exec_and_stop_commands[Commands.ACCELERATE] = Commands.STOP_ACCELERATE
exec_and_stop_commands[Commands.DECELERATE] = Commands.STOP_DECELERATE
exec_and_stop_commands[Commands.TURN_LEFT] = Commands.STOP_RIGHT
exec_and_stop_commands[Commands.TURN_RIGHT] = Commands.STOP_LEFT


def get_device():
    device = 'cpu'
    # if torch.cuda.is_available():
    #     device = 'cuda'
    return device