import fastai
from fastai.vision import *
import fastai.vision.models as models
import torch
import torchvision

import net


img_transformations = [torchvision.transforms.Resize(size=(320, 240)),
                       fastai.vision.transform.rand_resize_crop(size=(224, 224))]

img_data_bunch = ImageDataBunch.from_folder('/home/andre/Desktop/Arduino/Desktop-App/src/remote_car_controller/training/dataset',
                                            tfms=img_transformations)

def get_model(pretrained_model_func, **args):
    if not pretrained_model_func:
        raise ValueError('Missing pretrained_model_func')
    
    return pretrained_model_func(**args)


model = net.CNN(num_classes=5)
learner = create_cnn(data=img_data_bunch,
                     arch=lambda x: model,  # 224 * 244 * 3
                     pretrained=True,
                     metrics=[accuracy],
                     callback_fns=partial(GradientClipping, clip=5.0))  # Use attention if possible
# learner.freeze_to(-2)
fastai.train.to_fp16(learn=learner)
print(learner.get_preds())

# learner.freeze_to(-2)
# lr_find(learner, num_it=1)
# learn.recorder.plot()


    